# BioMechatronics Assignment 2 Group 1 Ocotonuts

Please find the folders split into ANN, SVM, and CNN. Inside each directory there will be additional files that will carry out different sections of a report.
### Important Files
1. Ranking Files - These are the most taxing files and will require computational power. They will run through each location and provide the top 15 features. 
2. location features - These files in each directory will run the classifier on that one location. It includes all the combinations that are possible within this location.
3. BigDataComb - This will compare each location to one another. This is done in order to find which location is the top segment. 
4. Confusion Matrix - These files allow you to run one specific location with a specific number of features from the dataset.
### How to Import dataset
We were given the zip files with all the dataset included. This just has to be unzipped and then the folder needs to be put in the same location as the file you are running. I have also included zip files of the ANN,SVM, and CNN folder used. This should include the Dataset with it data, which means upzip the files and then run whichever file you need. This can be found by unzipping Complete Zipped Directory.